<?php
/**
 * Special_offers_task
 *
 * @package   Special_offers_task
 * @author    Andrew Maiseyenka <nekaravaev@gmail.com>
 * @copyright 2021 Andrew Maiseyenka
 * @license   GPL 2.0+
 * @link      http://example.com
 */

/**
 * Get the settings of the plugin in a filterable way
 *
 * @since 1.0.0
 * @return array
 */
function sot_get_settings() {
	return apply_filters( 'sot_get_settings', get_option( SOT_TEXTDOMAIN . '-settings' ) );
}
