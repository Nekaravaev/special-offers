<?php
/**
 * Special_offers_task
 *
 * @package   Special_offers_task
 * @author    Andrew Maiseyenka <nekaravaev@gmail.com>
 * @copyright 2021 Andrew Maiseyenka
 * @license   GPL 2.0+
 * @link      http://example.com
 */

namespace Special_offers_task\Frontend;

use Special_offers_task\Engine\Base;

/**
 * Enqueue stuff on the frontend
 */
class Enqueue extends Base {

	/**
	 * Initialize the class.
	 *
	 * @return void
	 */
	public function initialize() {
		parent::initialize();

		// Load public-facing style sheet and JavaScript.
		\add_action( 'wp_enqueue_scripts', array( self::class, 'enqueue_styles' ) );
		\add_action( 'wp_enqueue_scripts', array( self::class, 'enqueue_scripts' ) );
		\add_action( 'wp_enqueue_scripts', array( self::class, 'enqueue_js_vars' ) );
	}


	/**
	 * Register and enqueue public-facing style sheet.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public static function enqueue_styles() {
		\wp_enqueue_style( SOT_TEXTDOMAIN . '-plugin-styles', \plugins_url( 'assets/css/public.css', SOT_PLUGIN_ABSOLUTE ), array(), SOT_VERSION );
	}


	/**
	 * Register and enqueues public-facing JavaScript files.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public static function enqueue_scripts() {
		\wp_enqueue_script( SOT_TEXTDOMAIN . '-plugin-script', \plugins_url( 'assets/js/public.js', SOT_PLUGIN_ABSOLUTE ), array( 'jquery' ), SOT_VERSION, false );
	}


	/**
	 * Print the PHP var in the HTML of the frontend for access by JavaScript.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public static function enqueue_js_vars() {
		\wp_localize_script(
			SOT_TEXTDOMAIN . '-plugin-script',
			'sot_js_vars',
			array(
				'alert' => \__( 'Hey! You have clicked the button!', SOT_TEXTDOMAIN ),
			)
		);
	}


}
