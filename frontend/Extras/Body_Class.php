<?php
/**
 * Plugin_name
 *
 * @package   Plugin_name
 * @author    Andrew Maiseyenka <nekaravaev@gmail.com>
 * @copyright 2021 Andrew Maiseyenka
 * @license   GPL 2.0+
 * @link      http://example.com
 */

namespace Special_offers_task\Frontend\Extras;

use Special_offers_task\Engine\Base;

/**
 * Add custom css class to <body>
 */
class Body_Class extends Base {

	/**
	 * Initialize the class.
	 *
	 * @return void
	 */
	public function initialize() {
		parent::initialize();

		\add_filter( 'body_class', array( self::class, 'add_sot_class' ), 10, 3 );
	}

	/**
	 * Add class in the body on the frontend
	 *
	 * @param array $classes The array with all the classes of the page.
	 * @since 1.0.0
	 * @return array
	 */
	public static function add_sot_class( array $classes ) {
		$classes[] = SOT_TEXTDOMAIN;

		return $classes;
	}

}
