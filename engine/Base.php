<?php

/**
 * Plugin_name
 *
 * @package   Plugin_name
 * @author    Andrew Maiseyenka <nekaravaev@gmail.com>
 * @copyright 2021 Andrew Maiseyenka
 * @license   GPL 2.0+
 * @link      http://example.com
 */

namespace Special_offers_task\Engine;

/**
 * Base skeleton of the plugin
 */
class Base {

	/**
	 * @var array The settings of the plugin.
	 */
	public $settings = array();

	/** Initialize the class and get the plugin settings */
	public function initialize() {
		$this->settings = \sot_get_settings();
	}

}
