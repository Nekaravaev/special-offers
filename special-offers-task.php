<?php

/**
 * @package   Special_offers_task
 * @author    Andrew Maiseyenka <nekaravaev@gmail.com>
 * @copyright 2021 Andrew Maiseyenka
 * @license   GPL 2.0+
 * @link      http://example.com
 *
 * Plugin Name:     Special offers task
 * Plugin URI:      @TODO
 * Description:     @TODO
 * Version:         1.0.0
 * Author:          Andrew Maiseyenka
 * Author URI:      http://example.com
 * Text Domain:     special-offers-task
 * License:         GPL 2.0+
 * License URI:     http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:     /languages
 * Requires PHP:    7.0
 * WordPress-Plugin-Boilerplate-Powered: v3.2.0
 */

// If this file is called directly, abort.
if ( !defined( 'ABSPATH' ) ) {
	die( 'We\'re sorry, but you can not directly access this file.' );
}

define( 'SOT_VERSION', '1.0.0' );
define( 'SOT_TEXTDOMAIN', 'special-offers-task' );
define( 'SOT_NAME', 'Special offers task' );
define( 'SOT_PLUGIN_ROOT', plugin_dir_path( __FILE__ ) );
define( 'SOT_PLUGIN_ABSOLUTE', __FILE__ );

if ( version_compare( PHP_VERSION, '7.0.0', '<=' ) ) {
	add_action(
		'admin_init',
		static function() {
			deactivate_plugins( plugin_basename( __FILE__ ) );
		}
	);
	add_action(
		'admin_notices',
		static function() {
			echo wp_kses_post(
			sprintf(
				'<div class="notice notice-error"><p>%s</p></div>',
				__( '"Special offers task" requires PHP 5.6 or newer.', SOT_TEXTDOMAIN )
			)
			);
		}
	);

	// Return early to prevent loading the plugin.
	return;
}

$special_offers_task_libraries = require_once SOT_PLUGIN_ROOT . 'vendor/autoload.php';

require_once SOT_PLUGIN_ROOT . 'functions/functions.php';

$requirements = new \Micropackage\Requirements\Requirements(
	'Special offers task',
	array(
		'php'     => '7.1',
		'plugins' => array(
			array('file' => 'advanced-custom-fields-pro/acf.php', 'name' => 'Advanced Custom Fields PRO')
		)
	)
);

if ( ! $requirements->satisfied() ) {
	$requirements->print_notice();

	return;
}




if ( ! wp_installing() ) {
	add_action(
		'plugins_loaded',
		static function () use ( $special_offers_task_libraries ) {
			new \Special_offers_task\Engine\Initialize( $special_offers_task_libraries );
		}
	);
}
